import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.controlsfx.control.CheckComboBox;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    public CheckBox ufwHandsSettings;
    public ComboBox<Option> ufwProtocol;
    public TextField ufwPort;
    public TextField ufwSender;
    public TextField ufwReciver;
    public TextField ufwCustom;
    public ComboBox<Option> ufwLogLevel;
    public ComboBox<Option> ufwDirection;
    public CheckComboBox<Option> protocolCombo;
    public TextField portsInput;
    public CheckBox checkNoChipher;
    public TextField customInput;
    public ComboBox<Option> interfaceCombo;
    public TextField proxyIp;
    public TextField proxyPort;
    public CheckBox proxyAutoBloc;
    public Button proxyStartButton;
    public TextArea blockedText;
    public TextField blockedCounter;


    public void start(){

    }

    public void addRule(){
        UFWController.Rule rule = UFWController.Rule.builder()
                .protocol(ufwProtocol.getValue().getValue())
                .port(ufwPort.getText())
                .sender(ufwSender.getText())
                .receiver(ufwReciver.getText())
                .custom(ufwCustom.getText())
                .logLevel(ufwLogLevel.getValue().getValue())
                .direction(ufwDirection.getValue().getValue())
                .build();
        Main.ufwController.addRule(rule);
    }


    public void startTshark(){
    }

    public void ufwSetMode(){
        boolean selected = ufwHandsSettings.isSelected();
        Main.ufwController.setMode(selected ? UFWController.Mode.AUTO : UFWController.Mode.HAND);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //UFW
        List<Option> optionList = new LinkedList<>();
        Option defaultOption = new Option("", "Все");
        optionList.add(defaultOption);
        optionList.add(new Option("proto tcp", "TCP"));
        optionList.add(new Option("proto udp", "UDP"));
        optionList.add(new Option("http", "HTTP"));
        optionList.add(new Option("dns", "DNS"));
        optionList.add(new Option("pop3", "POP3"));
        optionList.add(new Option("smtp", "SMTP"));
        ufwProtocol.setItems(FXCollections.observableList(optionList));
        ufwProtocol.setValue(defaultOption);
        optionList = new LinkedList<>();
        defaultOption = new Option("", "Отключены");
        optionList.add(defaultOption);
        optionList.add(new Option("log", "Новые соединения"));
        optionList.add(new Option("log-all", "Все соединения"));
        ufwLogLevel.setItems(FXCollections.observableList(optionList));
        ufwLogLevel.setValue(defaultOption);
        optionList = new LinkedList<>();
        defaultOption = new Option("in", "Входящий");
        optionList.add(defaultOption);
        optionList.add(new Option("out", "Исходящий"));
        ufwDirection.setItems(FXCollections.observableList(optionList));
        ufwDirection.setValue(defaultOption);

        //tshark
        optionList = new LinkedList<>();
        defaultOption = new Option("", "");
        optionList.add(defaultOption);
        optionList.add(new Option("any", "По всем"));
        optionList.add(new Option("emp3s0", "Ethernet"));
        optionList.add(new Option("lo", "Loopback"));
        interfaceCombo.setItems(FXCollections.observableList(optionList));
        interfaceCombo.setValue(defaultOption);

        optionList = new LinkedList<>();
        defaultOption = new Option("", "Все");
        optionList.add(defaultOption);
        optionList.add(new Option("icmp", "ICMP"));
        optionList.add(new Option("'tcp port 80'", "HTTP"));
        optionList.add(new Option("'port 53'", "DNS"));
        optionList.add(new Option("'tcp port 110'", "POP3"));
        optionList.add(new Option("'tcp port 25'", "SMTP"));
        optionList.add(new Option("'port 443'", "HTTPS"));
        protocolCombo.getItems().addAll(optionList);


    }

    public void startProxy(){
        if(Main.proxyController.isStarted()){
            Main.proxyController.stopProxy();
            proxyIp.setDisable(false);
            proxyPort.setDisable(false);
            proxyStartButton.setText("Запустить");
        }else {
            if (proxyIp.getText().isEmpty() || proxyPort.getText().isEmpty()) return;
            proxyIp.setDisable(true);
            proxyPort.setDisable(true);
            Main.proxyController.setBlockedCounterLabel(blockedCounter);
            Main.proxyController.setTextArea(blockedText);
            Main.proxyController.setCheckCrypto(proxyAutoBloc.isSelected());
            Main.proxyController.startProxy(proxyIp.getText(), Integer.parseInt(proxyPort.getText()));
            proxyStartButton.setText("Остановить");
        }
    }


    public void checkedBlocked(){
        Main.proxyController.setCheckCrypto(proxyAutoBloc.isSelected());
    }


}
