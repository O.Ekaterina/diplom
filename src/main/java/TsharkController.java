import java.io.IOException;
import java.util.List;

public class TsharkController {
    public static void start(List<String> protocols, String interfaceStr, String ports, String castom){
        String[] portsArr = ports.split(",");
        for(int i = 0; i < portsArr.length; i++){
            String[] p = portsArr[i].split(" ");
            protocols.add(""+p[0]+" port " + p[1]);
        }

        Runtime run = Runtime.getRuntime();
        StringBuilder command = new StringBuilder();
        command.append("tshark ")
                .append(interfaceStr.isEmpty()? "" : "-i " + interfaceStr + " ");
                for(int i = 0; i < protocols.size(); i++){
                    if(i != 0) command.append("or ");
                    command.append(protocols.get(i)).append(" ");
                }
                command.append(castom);
        try {
            Process exec = run.exec(new String[]{"bash", "-c", command.toString()});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
