import com.google.common.primitives.Bytes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import org.littleshoot.proxy.*;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.impl.ProxyUtils;
import org.littleshoot.proxy.mitm.CertificateSniffingMitmManager;
import org.littleshoot.proxy.mitm.RootCertificateException;

import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Test{
    static boolean sent = false;
    static DefaultHttpRequest defaultHttpRequest = null;
    public static void main(String[] args) throws RootCertificateException {
        HttpProxyServer server =
                DefaultHttpProxyServer.bootstrap()
                        .withAddress(new InetSocketAddress("192.168.0.136",8080))
                        .withFiltersSource(new HttpFiltersSourceAdapter() {

                            @Override
                            public int getMaximumRequestBufferSizeInBytes() {
                                return Integer.MAX_VALUE;
                            }

                            public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) {
                                return new HttpFiltersAdapter(originalRequest, ctx) {


                                    @Override
                                    public HttpResponse clientToProxyRequest(HttpObject httpObject) {
                                        // TODO: implement your filtering here
                                        if(httpObject instanceof FullHttpRequest)
                                        {
                                            FullHttpRequest httpRequest = (FullHttpRequest)httpObject;

                                            // How to access the POST Body data?
                                            if(httpRequest.content().readableBytes() != 0){
                                                byte[] b = new byte[httpRequest.content().readableBytes()];
                                                httpRequest.content().getBytes(0, b);
                                                String s = new String(b);
//                                                System.out.println(httpRequest.content().toString());
                                                System.out.println(s);
                                                System.out.println("cipher text: " + check(b));
//                                                return getBadGatewayResponse();
                                            }
                                        }
                                        return null;
                                    }

                                    @Override
                                    public HttpObject serverToProxyResponse(HttpObject httpObject) {
                                        // TODO: implement your filtering here
//                                        DefaultHttpRequest r = (DefaultHttpRequest) httpObject;
                                        return httpObject;
                                    }

                                };
                            }
                        })
                        .start();
        System.out.println(server.getListenAddress().getHostName());
    }

    private static HttpResponse getBadGatewayResponse() {
        String body = "<!DOCTYPE HTML \"-//IETF//DTD HTML 2.0//EN\">\n"
                + "<html><head>\n"
                + "<title>"+"Bad Gateway"+"</title>\n"
                + "</head><body>\n"
                + "Обнаружен незашифрованный текст!"
                + "</body></html>\n";
        byte[] bytes = body.getBytes(Charset.forName("UTF-8"));
        ByteBuf content = Unpooled.copiedBuffer(bytes);
        HttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_GATEWAY, content);
        response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, bytes.length);
        response.headers().set("Content-Type", "text/html; charset=UTF-8");
        response.headers().set("Date", ProxyUtils.formatDate(new Date()));
        response.headers().set(HttpHeaders.Names.CONNECTION, "close");
        return response;
    }

    static boolean check(byte[] b){
        int position = 0;
        int lastposition = 0;
        while(position != b.length -1){
            lastposition = position;
            position+=30;
            if(position>b.length -1) position = b.length -1;
            byte[] bytes = Arrays.copyOfRange(b, lastposition, position);
            if(!isCipherText(bytes)) return false;
        }
        return true;
    }

    static boolean isCipherText(byte[] b){
        long[] bytes = new long[256];
        for(int i = 0; i < 256; i++){
            bytes[i] = 0;
        }
        long encrCounter = 0;
        for(int i = 0; i < b.length; i++){
            encrCounter++;
            ++bytes[(b[i])+128];
        }
        long max = 0;
        long min = Long.MAX_VALUE;
        for (long num : bytes){
            if(num > max) max = num;
            if(num < min) min = num;
        }
        System.out.println("count: " + encrCounter);
        System.out.println("min: " + min);
        System.out.println("max: " + max);
        System.out.println(max * 1.0 / encrCounter - min * 1.0 / encrCounter);
        return (max * 1.0 / encrCounter - min * 1.0 / encrCounter) <= 0.07;
    }

}