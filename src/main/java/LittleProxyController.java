import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.impl.ProxyUtils;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

public class LittleProxyController {
    private boolean CheckCrypto = false;
    private HttpProxyServer server;
    private boolean started = false;
    private TextArea textArea;
    private TextField blockedCounterLabel;
    private int blockedCounter = 0;

    public void startProxy(String ip, int port) {
        server = DefaultHttpProxyServer.bootstrap()
                .withAddress(new InetSocketAddress(ip, port))
                .withFiltersSource(new HttpFiltersSourceAdapter() {

                    @Override
                    public int getMaximumRequestBufferSizeInBytes() {
                        return Integer.MAX_VALUE;
                    }

                    public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) {
                        return new HttpFiltersAdapter(originalRequest, ctx) {

                            @Override
                            public HttpResponse clientToProxyRequest(HttpObject httpObject) {
                                if (isCheckCrypto()) {
                                    if (httpObject instanceof FullHttpRequest) {
                                        FullHttpRequest httpRequest = (FullHttpRequest) httpObject;
                                        if (httpRequest.content().readableBytes() != 0) {
                                            byte[] b = new byte[httpRequest.content().readableBytes()];
                                            httpRequest.content().getBytes(0, b);
                                            String s = new String(b);
                                            if (!check(b)) return getBadGatewayResponse();
                                        }
                                    }
                                }
                                return null;
                            }
                        };
                    }
                })
                .start();
        System.out.println("proxy started");
        started = true;
        blockedCounter = 0;
    }

    public void stopProxy() {
        server.stop();
        System.out.println("proxy stop");
        started = false;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    private HttpResponse getBadGatewayResponse() {
        String body = "<!DOCTYPE HTML \"-//IETF//DTD HTML 2.0//EN\">\n"
                + "<html><head>\n"
                + "<title>" + "Bad Gateway" + "</title>\n"
                + "</head><body>\n"
                + "Обнаружен незашифрованный текст!"
                + "</body></html>\n";
        byte[] bytes = body.getBytes(Charset.forName("UTF-8"));
        ByteBuf content = Unpooled.copiedBuffer(bytes);
        HttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_GATEWAY, content);
        response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, bytes.length);
        response.headers().set("Content-Type", "text/html; charset=UTF-8");
        response.headers().set("Date", ProxyUtils.formatDate(new Date()));
        response.headers().set(HttpHeaders.Names.CONNECTION, "close");
        return response;
    }

    private boolean check(byte[] b) {
        int position = 0;
        int lastposition = 0;
        while (position != b.length - 1) {
            lastposition = position;
            position += 30;
            if (position > b.length - 1) position = b.length - 1;
            byte[] bytes = Arrays.copyOfRange(b, lastposition, position);
            if (!isCipherText(bytes)) {
                if(textArea != null) textArea.setText(new String(b));
                blockedCounter += 1;
                if(blockedCounterLabel != null) blockedCounterLabel.setText("" + blockedCounter);
                System.out.println("cipher text: " + false);
                return false;
            }
        }
        System.out.println("cipher text: " + true);
        return true;
    }

    private boolean isCipherText(byte[] b) {
        long[] bytes = new long[256];
        for (int i = 0; i < 256; i++) {
            bytes[i] = 0;
        }
        long encrCounter = 0;
        for (int i = 0; i < b.length; i++) {
            encrCounter++;
            ++bytes[(b[i]) + 128];
        }
        long max = 0;
        long min = Long.MAX_VALUE;
        for (long num : bytes) {
            if (num > max) max = num;
            if (num < min) min = num;
        }
        System.out.println("count: " + encrCounter);
        System.out.println("min: " + min);
        System.out.println("max: " + max);
        System.out.println(max * 1.0 / encrCounter - min * 1.0 / encrCounter);
        return !((max * 1.0 / encrCounter - min * 1.0 / encrCounter) > 0.07);
    }


    public boolean isCheckCrypto() {
        return CheckCrypto;
    }

    public void setCheckCrypto(boolean checkCrypto) {
        CheckCrypto = checkCrypto;
    }

    public boolean isStarted() {
        return started;
    }


    public void setBlockedCounterLabel(TextField blockedCounterLabel) {
        this.blockedCounterLabel = blockedCounterLabel;
    }
}
