import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Setter
public class UFWController {
    private Mode mode = Mode.HAND;

    public ChoiceBox ufwProtocol;
    public TextField ufwPort;
    public TextField ufwSender;
    public TextField ufwReceiver;
    public TextField ufwCustom;
    public ComboBox ufwLogLevel;

    public void addRule(Rule rule){
        Runtime run = Runtime.getRuntime();
        StringBuilder command = new StringBuilder();
        command.append("ufw ")
                .append("deny ")
                .append(rule.getDirection()).append(" ")
                .append(rule.getLogLevel()).append(" ")
                .append(rule.getProtocol().isEmpty()? "" : rule.getProtocol() + " ")
                .append(rule.getSender().isEmpty() ? "" : "from " + rule.getSender() + " ")
                .append(rule.getReceiver().isEmpty() ? "" : "to " + rule.getReceiver() + " ")
                .append(rule.getPort().isEmpty() ? "" : "port " + rule.getPort() + " ");
        try {
            Process exec = run.exec(new String[]{"bash", "-c", command.toString()});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enable(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw enable"});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void disable(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw disable"});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void reset(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw reset"});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void logOn(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw logging on"});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void logOff(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw logging off"});
            exec.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<Integer>  numbers(){
        Runtime run = Runtime.getRuntime();
        try {
            Process exec = run.exec(new String[]{"bash", "-c", "ufw status numbered"});
            InputStream inputStream = exec.getInputStream();
            long time = new Date().getTime();
            List<Integer> numbers = new ArrayList<>();
            final String regex = "\\[\\s*(\\d*)\\]";
            final Pattern pattern = Pattern.compile(regex);
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line = bufferedReader.readLine();
                while (line != null) {
                    final Matcher matcher = pattern.matcher(line);
                    if(matcher.find()){
                        int i = Integer.parseInt(matcher.group(1));
                        numbers.add(i);
                    }
                    line = bufferedReader.readLine();
                }
            }
            exec.waitFor();
            return numbers;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getNextNumber(){
        List<Integer> numbers = numbers();
        if(numbers == null || numbers.size() == 0){
            return 0;
        }else {
            return numbers.stream().max(Integer::compare).get();
        }
    }









    public static enum Mode{
        HAND,
        AUTO
    }

    @Data
    @Builder
    public static class Rule{
        long number;
        String protocol;
        String port;
        String sender;
        String receiver;
        String custom;
        String logLevel;
        String direction;
    }




}

