import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    static Stage mainStage;
    static UFWController ufwController;
    static LittleProxyController proxyController;
    @Override
    public void start(Stage primaryStage) throws Exception {
        ufwController = new UFWController();
        proxyController = new LittleProxyController();
        Main.mainStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("MainController.fxml"));
        mainStage.setTitle("tshark");
        Scene scene = new Scene(root);
        scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
        mainStage.setScene(scene);
        mainStage.show();
    }
}
