import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
public class Option {
    private String value;
    private String text;

    @Override
    public String toString() {
        return text;
    }
}
